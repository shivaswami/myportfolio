import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()
export class MainServiceService {

  constructor() { }

  games = [
    {
      "id":1,
      "name": "NFS Most Wanted",
      "version": 2,
      "price": 150,
      "game":"nfs",
      "img": "./assets/images/games.jpg",
      "players": 1
    },
    {
      "id":2,
      "name": "Tic Tak Toy",
      "version": 1,
      "price": 100,
       "game":"ttt",
      "img": "./assets/images/ttt.jpg",
      "players": 2
    }
    // {
    //   "id":3,
    //   "name": "Sea Of Thieves",
    //   "version": 8,
    //   "price": 200,
    //    "game":"sot",
    //   "img": "./assets/images/seaofthievs.jpg",
    //   "players": 3
    // }
  ]


  projects = [
    {
      "id":1,
      "name": "Patients Tracking System",
      "developer": "Shiv Swamy",
      "img": "./assets/images/patients.jpg",
      "duration": 5
    }
    // {
    //   "id":2,
    //   "name": "Event Management",
    //   "developer": "Shwetha",
    //   "img": "./assets/images/event.jpg",
    //   "duration": 3
    // }
    // {
    //   "id":3,
    //   "name": "Banking App",
    //    "developer": "Sharan", 
    //   "img": "./assets/images/bankapp.jpg",
    //   "duration": 4
    // }
  ]

}
