import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AboutmeComponent } from './aboutme/aboutme.component';
import { SkillsetComponent } from './skillset/skillset.component';
import { GetintouchComponent } from './getintouch/getintouch.component';
import { TictaktoyComponent } from './tictaktoy/tictaktoy.component';
import { HomeComponent } from './home/home.component';
import { SOTGameComponent } from './sotgame/sotgame.component';
import { NFSGameComponent } from './nfsgame/nfsgame.component';
import { WeddingComponent } from './wedding/wedding.component';
//custom classes
import { ReverseStr } from './reverseString';
import { HilightDirective } from './highlight';

import { MainServiceService } from './main-service.service';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AboutmeComponent,
    SkillsetComponent,
    GetintouchComponent,
    TictaktoyComponent,
    HomeComponent,
    NFSGameComponent,
    SOTGameComponent,
    ReverseStr,
    HilightDirective,
    WeddingComponent

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'ttt', component: TictaktoyComponent },
      { path: 'nfs', component: NFSGameComponent },
      { path: 'sot', component: SOTGameComponent },
      { path: 'home/wedding', component: WeddingComponent },
      { path: 'phome', loadChildren: './patients/patients.module#PatientsModule' },
    ])
  ],
  providers: [MainServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
