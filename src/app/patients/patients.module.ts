import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from '@angular/forms';


import { PatientshomeComponent } from './patientshome/patientshome.component';
import { AddpatientsComponent } from './addpatients/addpatients.component';
import { PatientslistComponent } from './patientslist/patientslist.component';
import { UpdatepatientComponent } from './updatepatient/updatepatient.component';
import { LoginComponent } from './login/login.component';


import { PatientServiceService } from './patient-service.service';
import { AuthGuard } from './auth-guard.service';




@NgModule({
  imports: [
    CommonModule, HttpClientModule, ReactiveFormsModule, RouterModule.forChild([
      { path: '', redirectTo: 'phome', pathMatch: 'full' },
      { path: 'phome', component: PatientshomeComponent},
      { path: 'addpatient', component: AddpatientsComponent},
      { path: 'plist', component: PatientslistComponent},
      { path: 'login', component: LoginComponent},
      { path: 'updatep/:id', component: UpdatepatientComponent },
    ])
  ],
  declarations: [PatientshomeComponent, AddpatientsComponent, PatientslistComponent, UpdatepatientComponent, LoginComponent],
  providers: [PatientServiceService,AuthGuard]
})
export class PatientsModule { }
