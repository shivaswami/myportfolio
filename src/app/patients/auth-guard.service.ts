import { Injectable } from '@angular/core';
import { CanActivate ,Router} from '@angular/router';

import {LoginComponent} from './login/login.component';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router:Router) { }

  canActivate() {
    return false;
  }

}
