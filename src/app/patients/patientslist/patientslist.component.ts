import { Component, OnInit } from "@angular/core";
import { HttpClient, HttpResponse, HttpHeaders } from "@angular/common/http";


@Component({
  selector: 'app-patientslist',
  templateUrl: './patientslist.component.html',
  styleUrls: ['./patientslist.component.css']
})
export class PatientslistComponent implements OnInit {
base_url = "http://localhost:3000/patients";
  patients;

  private headers = new HttpHeaders({ "Content-Type": "application/json" });
  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.getData();
  }
  getData() {
    this.http.get(this.base_url).subscribe(res => {
      this.patients = res;
    });
  }
  deletePatient(id) {
    if (confirm("Are Sure?")) {
      const url = `${this.base_url}/${id}`;
      return this.http
        .delete(url, { headers: this.headers })
        .toPromise()
        .then(() => {
          this.getData() ;
        });
    }
  }

}
