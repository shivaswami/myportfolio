import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  isLoggedIn = false;
  username: string="shiv";
  password: string="welcome";
  form: FormGroup;
  constructor(fb: FormBuilder, private route: Router) {
    this.form = fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  login() {
    this.isLoggedIn = false;
    this.username = this.form.value['username'];
    this.password = this.form.value['password'];
    this.username.toLowerCase();
    this.password.toLowerCase();
    if (this.username === "shiv" || this.username === "SHIV" && this.password === "welcome" || this.password === "WELCOME" ) {
      this.route.navigateByUrl('phome/phome');
      this.isLoggedIn = true;
    }
    else {
      this.isLoggedIn = true;
      alert("plese enter 'shiv' as name and 'welcome' as password")
    }
    return this.isLoggedIn;
  }
}
