import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { map } from "rxjs/operators";
import { ActivatedRoute, Router } from '@angular/router';


@Injectable()
export class PatientServiceService {

  constructor(private http: HttpClient, private routes: Router) { }
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  base_url = "http://localhost:3000/patients";
  patients;
  
  gender={
    "male":"M",
    "female":"F"}
  
  getPatients() {
    return this.http.get(this.base_url).pipe(map(res => res));
  }

  addPatientDetails(patientObj) {
    return this.http.post(this.base_url, patientObj);
  }

  updatePatient(url, patientObj, headers) {
    return this.http.put(url, patientObj, headers)
      .toPromise()
      .then((res) => {
        this.routes.navigateByUrl('phome/phome');
      })
  }

}
