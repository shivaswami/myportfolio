import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PatientServiceService } from '../patient-service.service';


@Component({
  selector: 'app-addpatients',
  templateUrl: './addpatients.component.html',
  styleUrls: ['./addpatients.component.css']
})
export class AddpatientsComponent implements OnInit {

  addpatientsForm: FormGroup;
  patientObj = {};
  isAdded;
  selectGender;
  constructor(private http: HttpClient, private route: Router, private fb: FormBuilder, private service: PatientServiceService) {

    this.addpatientsForm = fb.group({
      fname: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(24), Validators.pattern('^[a-zA-Z ]*$')]],
      lname: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(24), Validators.pattern('^[a-zA-Z ]*$')]],
      age: ['', [Validators.required, Validators.maxLength(3), Validators.pattern('^[0-9]*$'), this.minMax]],
      gender: ['', Validators.required],
      address: ['', [Validators.required, Validators.maxLength(1000)]],
      phone: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern('^[0-9]*$')]],
      consultedBy: ['', Validators.required],
      complaints: ['', [Validators.required, Validators.maxLength(1000)]]
    });
  }

  ngOnInit() {
    this.selectGender = this.service.gender;
  }


  //age validation
  minMax(control: FormControl) {
    return (control.value) >= 1 && (control.value) <= 100 ? null : {
      minMax: true
    }
  }
  //  add patiesnts
  addPatient(patient) {
    this.patientObj = {
      "fname": patient.fname,
      "lname": patient.lname,
      "age": patient.age,
      "gender": patient.gender,
      "address": patient.address,
      "phone": patient.phone,
      "consultedBy": patient.consultedBy,
      "complaints": patient.complaints
    }
    if (this.addpatientsForm.valid) {
      this.service.addPatientDetails(this.patientObj)
        .subscribe((res) => {
          this.route.navigateByUrl('phome/phome')
        })
    }
  }



}
