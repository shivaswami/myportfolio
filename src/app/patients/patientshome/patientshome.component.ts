import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { PatientServiceService } from '../patient-service.service';


@Component({
  selector: 'app-patientshome',
  templateUrl: './patientshome.component.html',
  styleUrls: ['./patientshome.component.css']
})
export class PatientshomeComponent implements OnInit {

  patients;
  total: number;
  isLoading: boolean = true;


  constructor(private http: HttpClient, public service: PatientServiceService) { }


  ngOnInit() {

    this.service.getPatients().subscribe(res => {
      this.isLoading = false;
      this.patients = res;
      this.total = this.patients.length;
    })

  }
}

