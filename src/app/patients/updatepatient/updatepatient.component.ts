import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { PatientServiceService } from '../patient-service.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-updatepatient',
  templateUrl: './updatepatient.component.html',
  styleUrls: ['./updatepatient.component.css']
})
export class UpdatepatientComponent implements OnInit {

  base_url = "http://localhost:3000/patients";
  patients;
  id: number;
  data;
  patientObj = {};
  updatepatientsForm: FormGroup
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private route: ActivatedRoute, private fb: FormBuilder, private routes: Router, private service: PatientServiceService) {
    this.updatepatientsForm = fb.group({
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      age: ['', Validators.required],
      gender: ['', Validators.required],
      address: ['', Validators.required],
      phone: ['', Validators.required],
      consultedBy: ['', Validators.required],
      result: ['', Validators.required],
      prescription: ['', Validators.required],
      complaints: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
    })
    this.service.getPatients().subscribe(res => {
      this.patients = res;
      for (var i = 0; i < this.patients.length; i++) {
        if (this.patients[i].id === this.id) {
          this.data = this.patients[i];
          break;
        }
      }
    })
  }

  updatePatient(patient) {
    this.patientObj = {
      "fname": patient.fname,
      "lname": patient.lname,
      "age": patient.age,
      "gender": patient.gender,
      "address": patient.address,
      "phone": patient.phone,
      "consultedBy": patient.consultedBy,
      "complaints": patient.complaints,
      "result": patient.result,
      "prescription": patient.prescription
    }
    const url = `${this.base_url}/${this.id}`;
    this.service.updatePatient(url, JSON.stringify(this.patientObj), { headers: this.headers })
  }

}
