import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TictaktoyComponent } from './tictaktoy.component';

describe('TictaktoyComponent', () => {
  let component: TictaktoyComponent;
  let fixture: ComponentFixture<TictaktoyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TictaktoyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TictaktoyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
