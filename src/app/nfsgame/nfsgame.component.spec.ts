import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NFSGameComponent } from './nfsgame.component';

describe('NFSGameComponent', () => {
  let component: NFSGameComponent;
  let fixture: ComponentFixture<NFSGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NFSGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NFSGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
