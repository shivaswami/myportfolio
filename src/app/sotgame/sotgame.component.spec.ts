import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SOTGameComponent } from './sotgame.component';

describe('SOTGameComponent', () => {
  let component: SOTGameComponent;
  let fixture: ComponentFixture<SOTGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SOTGameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SOTGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
