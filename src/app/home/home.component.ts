import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MainServiceService } from '../main-service.service';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  isShowingProjs: boolean;
  isShowingGames: boolean = false;
  isShowingWeddingApp:boolean;
  projects;
  games;
  id: number;
  date = new Date();
  dd = this.date.getDate();
  mm = this.date.getMonth();
  yy = this.date.getFullYear();
  constructor(private route: Router, private routes: ActivatedRoute, private service: MainServiceService) { }

  ngOnInit() {
    //    this.routes.params.subscribe(params => {
    //   this.id = +params['id'];  

    // })

    this.showAll();
    this.getPorjects();
    this.getGames();
    this.scrollUp();
    this.typeWriter();
  }
  showAll() {
    this.isShowingGames = true;
    this.isShowingProjs = true;
    this.isShowingWeddingApp=true;
  }
  showGames(id) {
    this.id = id;
    this.isShowingGames = true;
    this.isShowingProjs = false;
      this.isShowingWeddingApp=false;
  }
  showProjects(id) {
    this.isShowingGames = false;
    this.isShowingProjs = true;
      this.isShowingWeddingApp=false;
  }
  getPorjects() {
    this.projects = this.service.projects;
  }
  getGames() {
    this.games = this.service.games;
  }

  //scrollup
  scrollUp() {
    if ($('#back-to-top').length) {
      var scrollTrigger = 500, // px
        backToTop = function () {
          var scrollTop = $(window).scrollTop();
          if (scrollTop > scrollTrigger) {
            $('#back-to-top').addClass('show');
          } else {
            $('#back-to-top').removeClass('show');
          }
        };
      backToTop();
      $(window).on('scroll', function () {
        backToTop();
      });
      $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
          scrollTop: 0
        },2000);
      });
    }
  }

  //typewriter

  typeWriter() {
    var _CONTENT = [
      "  SHIV SWAMY.",
      "   DESIGNER.",
      "   DEVELOPER.",
      "   FREELANCER."
    ];

    // Current sentence being processed
    var _PART = 0;

    // Character number of the current sentence being processed 
    var _PART_INDEX = 0;

    // Holds the handle returned from setInterval
    var _INTERVAL_VAL;

    // Element that holds the text
    var _ELEMENT = document.querySelector("#text");

    // Cursor element 
    var _CURSOR = document.querySelector("#cursor");

    // Implements typing effect
    function Type() {
      // Get substring with 1 characater added
      var text = _CONTENT[_PART].substring(0, _PART_INDEX + 1);
      _ELEMENT.innerHTML = text;
      _PART_INDEX++;

      // If full sentence has been displayed then start to delete the sentence after some time
      if (text === _CONTENT[_PART]) {
        // Hide the cursor
     //   _CURSOR.style.display = 'none';

        clearInterval(_INTERVAL_VAL);
        setTimeout(function () {
          _INTERVAL_VAL = setInterval(Delete, 50);
        }, 1000);
      }
    }

    // Implements deleting effect
    function Delete() {
      // Get substring with 1 characater deleted
      var text = _CONTENT[_PART].substring(0, _PART_INDEX - 1);
      _ELEMENT.innerHTML = text;
      _PART_INDEX--;

      // If sentence has been deleted then start to display the next sentence
      if (text === '') {
        clearInterval(_INTERVAL_VAL);

        // If current sentence was last then display the first one, else move to the next
        if (_PART == (_CONTENT.length - 1))
          _PART = 0;
        else
          _PART++;

        _PART_INDEX = 0;

        // Start to display the next sentence after some time
        setTimeout(function () {
        //  _CURSOR.style.display = 'inline-block';
          _INTERVAL_VAL = setInterval(Type, 100);
        }, 200);
      }
    }

    // Start the typing effect on load
    _INTERVAL_VAL = setInterval(Type, 100);
  }


}
